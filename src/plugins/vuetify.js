import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary_text: '#333333',
                secondary_text: '#666666',
                primary: '#304c7d',
                secondary: '#407EC9',
                action: '#ff9800',
                gray_20: '#333333',
                gray: '#95979A',
                success: '#43B02A',
                accent: '#43B02A',
                error: '#ff0000',
                background: '#f6f6f6',

                // date picker custom colors
                date_picker_disabled: '#cccccc',

                // colors for specialities
                pantone_144_c : '#E0863C',
                pantone_361_c : '#5DAD57',
                pantone_660_c : '#4470B3',
                pantone_348_c : '#398049',
                pantone_538_c : '#AEB34E',
                pantone_326_c : '#52B5AF',
                pantone_682_c : '#914D73',
                pantone_2995_c : '#4AA6DB',
                pantone_1235_c : '#F3B946',
                pantone_cool_gray_7_c : '#95979A',
                pantone_process_cyan_c : '#449CD5',
                pantone_blue_072_c : '#2C3487',
            },         
        },
    }    
});
