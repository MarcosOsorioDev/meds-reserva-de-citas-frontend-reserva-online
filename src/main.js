import Vue      from 'vue'
import App      from '@/App.vue'
import router   from '@/router'
import vuetify  from '@/plugins/vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import VueAnalytics from 'vue-analytics';

// Vuex Store
import store from '@/store/store'

Vue.config.productionTip = false

Vue.use(VueAnalytics, {
  id: 'UA-15478204-1',
  router
})

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
