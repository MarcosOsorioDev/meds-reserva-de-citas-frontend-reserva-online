import Vuex from 'vuex'
import Vue from 'vue'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

const store = new Vuex.Store({
    // persisted state using localstorage
    // https://github.com/robinvdvleuten/vuex-persistedstate
    plugins: [createPersistedState()],    

    state: {
        init : {
            // página iniciada con parámetros
            telemedicina: false,
        },        
        user : {
            // user data managed through navigation
            rut: null,
            name: null,
            email: null, 
            phone: null,
            gender: null,
            birthday: null,
        },
        selections: {
            // selections that the user has made when using the app
            specialty_id: null,
            specialty_name: null,
            professional_rut: null,
            professional_specialty_id: null,
            professional_specialty_name: null,
            professional_name: null,
            center_name: null,
            center_id: null,
            appointment_date: null, 
            appointment_hour: null,
            appointment_center_id: null,
        },
        current: {
            // active properties that user is visiting
            professionalCV: null,
        },        
        layout : {
            // layout size for main elements (Responsiveness)
            mainCard : {    
                'xs':12,
                'sm':10,
                'md':8,
                'lg':6,
                'xl':4,
            },
            userInputs : {
                // layout size for user input fields and other form elements
                'cols' : 10,
                'sm' :8
            },
            topRowDense : true,
        },
        shape : {
            // shape for the main containers
            cardTile : false,
        }
      },
      
      getters: {
        // SELECTIONS GETTERS
        getSelectedCenterId: (state) => {
            // retrieves stored center id
            return state.selections.center_id
        },
        getSelectedCenterName: (state) => {
            // retrieves stored center name
            return state.selections.center_name
        },            
        getSelectedSpecialtyId: (state) => {
            // retrieves stored speciality id
            return state.selections.specialty_id
        },
        getSelectedSpecialtyName: (state) => {
            // retrieves stored speciality name
            return state.selections.specialty_name
        },
        getSelectedProfessionalRut: (state) => {
            // retrieves stored professional rut
            return state.selections.professional_rut
        },
        getSelectedProfessionalName: (state) => {
            // retrieves stored professional name
            return state.selections.professional_name
        },        
      },
    
      mutations: {
        // INITIALIZATION MUTATIONS
        setInitTelemedicina (state, active) {            
            // storing rut in user store
            state.init.telemedicina = active
        },
        // USER MUTATIONS
        setUserRut (state, rut) {            
            // storing rut in user store
            state.user.rut = rut
        },
        setUserName (state, name) {
            // storing name in selections
            state.selections.name = name
            // storing name in user store
            state.user.name = name            
        },
        setUserEmail (state, email) {
            // storing email in user store
            state.user.email = email            
        },
        setUserPhone (state, phone) {
            // storing phone in user store
            state.user.phone = phone            
        },
        setUserGender (state, gender) {
            // storing gender in user store
            state.user.gender = gender            
        },
        setUserBirthday (state, birthday) {
            // storing birthday in user store
            state.user.birthday = birthday            
        },

        // SELECTIONS MUTATIONS
        setSelectedSpecialty(state, specialty) {            
            // storing specialty in selection store
            state.selections.specialty_id = specialty.id
            state.selections.specialty_name = specialty.name
        },
        setSelectedProfessional(state, professional) {            
            // storing specialty in selection store
            state.selections.professional_rut = professional.rut
            state.selections.professional_name = professional.name
            state.selections.professional_specialty_id = professional.specialty_id
            state.selections.professional_specialty_name = professional.specialty_name
        },        
        setSelectedCenter (state, center) {            
            // storing center in selection store
            state.selections.center_id = center.id
            state.selections.center_name = center.name
        },        
        setSelectedDate (state, fullDate) {            
            // storing date and hour in selection store
            state.selections.appointment_date = fullDate.selectedDate
            state.selections.appointment_hour = fullDate.selectedHour
            state.selections.appointment_center_id = fullDate.selectedCenter
        },
        setActiveProfessionalCV (state, cv) {            
            // storing date and hour in selection store
            state.current.professionalCV = cv
        },        
      },  
  })

export default store