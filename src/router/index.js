// Base Imports
import Vue from 'vue'
import VueRouter from 'vue-router'

// Login Process
import CustomerLogin                from '@/views/login/CustomerLogin.vue'

// Search Process
import Search                       from '@/views/search/Search.vue'
import SearchCenters                from '@/views/search/SearchCenters.vue'
import SearchSpecialities           from '@/views/search/SearchSpecialities.vue'
import SearchProfessionals          from '@/views/search/SearchProfessionals.vue'

// Professional Detail
import Professional                 from '@/views/professional/Professional.vue'
import ProfessionalBookingCalendar  from '@/views/professional/ProfessionalBookingCalendar.vue'
import ProfessionalBookingList      from '@/views/professional/ProfessionalBookingList.vue'
import ProfessionalInformation      from '@/views/professional/ProfessionalInformation.vue'

// Confirmation Process
import HourConfirmation             from '@/views/confirmation/HourConfirmation.vue'
import HourSuccess                  from '@/views/confirmation/HourSuccess.vue'

Vue.use(VueRouter)

const routes = [
  {
    // first and default route
    path: '/',
    component: CustomerLogin
  },
  {
    // login route
    path: '/login',
    component: CustomerLogin
  },
  {
    // login route
    path: '/telemedicina',
    component: CustomerLogin
  },  
  {
    // search route
    path: '/search',
    component: Search,
    children: [
      {
        path: '',
        component: SearchSpecialities
      },      
      {
        path: 'centers',
        component: SearchCenters
      },
      {
        path: 'specialities',
        component: SearchSpecialities
      },      
      {
        path: 'professionals',
        component: SearchProfessionals
      }
    ]
  },
  {
    path: '/professional',
    component: Professional,
    children: [
      {
        path: '',
        component: ProfessionalBookingList
      },
      {
        path: 'booking_calendar',
        component: ProfessionalBookingCalendar
      },        
      {
        path: 'booking_list',
        component: ProfessionalBookingList
      },    
      {
        path: 'information',
        component: ProfessionalInformation
      }
    ]
  },
  {
    path: '/booking/confirm',
    component: HourConfirmation,
  },
  {
    path: '/booking/success',
    component: HourSuccess,
  },   
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

export default router
